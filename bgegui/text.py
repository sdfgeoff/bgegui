'''This module provides support for text entry boxes. The text entry box
has support for various modes including:
 - Text only
 - Text and numbers only
 - Numbers only
 - Integers only
'''
import time
import re

import bge
from . import node


SETTABLE_VALUES = [
    'inputtype',  # Should be one of 'float', 'int', 'all'
    'maxlines',  # Maximum number of lines
    'maxlength',  # Maximum number of characters
    'value',  # The initial text of the object
    'active',  # If it can be edited
]

CALLBACKS = [
    'onchange',  # Get's run whenever the value changes
]


FLASH_RATE = 0.5
CURSOR_CHARACTER = '|'  # Note that this character is unable to be typed

INPUT_TYPE_FILTERS = {
    'float': re.compile(r'[^\d.^-]+'),
    'int': re.compile(r'[^\d^-]')
}


def make_text(cont):
    '''Makes the current object into a text entry line'''
    node.set_gui_element_class(cont.owner, TextWidget(cont.owner))


class TextWidget(object):
    '''A text widget that can be clicked on an typed into'''
    maxlines = -1
    maxlength = -1
    _inputtype = 'all'
    active = True

    def __init__(self, obj):
        self.has_cursor = False
        self.obj = obj
        self.node = node.EmptyNode(obj)
        self.node.onactive = self.flash_cursor
        self.node.ondeactivate = self.remove_cursor
        self.node.onkeypress = self.type_text
        self.text_obj = None
        for child in obj.children:
            if isinstance(child, bge.types.KX_FontObject):
                self.text_obj = child
        node.set_attrs(self, CALLBACKS, SETTABLE_VALUES)
        if self.obj.groupObject is not None:
            self.obj.setParent(self.obj.groupObject)

    def onchange(self, obj, val):
        '''Runs on change'''

    def flash_cursor(self, *_):
        '''Flashes the cursor by adding a character to the end'''
        cursor_visible = time.time() % FLASH_RATE > FLASH_RATE / 2
        if not self.active:
            cursor_visible = False
        if cursor_visible and not self.has_cursor:
            self.text_obj.text += CURSOR_CHARACTER
            self.has_cursor = True
        if not cursor_visible:
            self.remove_cursor()

    def remove_cursor(self, *_):
        '''Removes the cursor from the text field'''
        if self.has_cursor:
            old_text = self.text_obj.text
            self.text_obj.text = old_text.replace(CURSOR_CHARACTER, '')
            self.has_cursor = False

    def type_text(self, _, keyevent):
        '''Types text into the text box'''
        if not self.active:
            return
        if self.has_cursor:
            self.remove_cursor()
        caps = keyevent.shiftKey
        for button in keyevent.buttons:
            self.value += bge.events.EventToCharacter(button, caps)

        if bge.events.BACKSPACEKEY in keyevent.buttons:
            self.value = self.value[:-1]

    @property
    def inputtype(self):
        return self._inputtype

    @inputtype.setter
    def inputtype(self, val):
        self._inputtype = val
        self.value = self.check_constraints(self.value)

    @property
    def value(self):
        '''Returns the contents of the text cell'''
        self.remove_cursor()
        text = self.text_obj.text
        self.flash_cursor()
        return text

    @value.setter
    def value(self, val):
        val = self.check_constraints(val)
        self.text_obj.text = val
        self.onchange(self, val)

    def check_constraints(self, val):
        val = str(val)
        if self.maxlines > 0:
            val = '\n'.join(val.split('\n')[:self.maxlines])
        if self.inputtype in INPUT_TYPE_FILTERS:
            val = INPUT_TYPE_FILTERS[self.inputtype].sub('', val)
        if self.maxlength > 0:
            val = val[:self.maxlength]
        return val
