'''The select box offers a way of selecting a value from a list

Note that you can get the current value, but to set a value, you have to use
the .index property
'''

import time

from . import node

SETTABLE_VALUES = [
    'index',  # The index in selectlist for the default value
    'selectlist', # A comma-separated string of selectable values
    'active'
]

CALLBACKS = [
    'onchange',  # Get's run whenever the value changes
]

def make_selectbox(cont):
    '''Turns an object into a numberbox'''
    new_widget = NumberBox(cont.owner)
    node.set_gui_element_class(cont.owner, new_widget)


class NumberBox(object):
    _index = 0
    active = True
    def __init__(self, obj):
        self.buttons = list()
        self.obj = obj
        self.selectlist = list()
        for child in obj.children:
            if 'selectboxbutton' in child:
                button_callbacks = {
                    'onclick': self.press,
                    'onmousedown': self.hold,
                }
                new_node = node.EmptyNode(child, button_callbacks)
                self.buttons.append(new_node)
        node.set_attrs(self, CALLBACKS, SETTABLE_VALUES)
        if isinstance(self.selectlist, str):
            self.selectlist = self.selectlist.split(',')
        self.index = self.index

        if self.obj.groupObject is not None:
            self.obj.setParent(self.obj.groupObject)
        if node.get_gui_element(self.obj) is not None:
            node.get_gui_element(self.obj).active = False
        else:
            self.obj['active'] = False


    def press(self, button, event):
        '''Runs when the mouse button is pressed on one of the buttons'''
        if self.active:
            button.obj['click_time'] = (time.time() % 1000)
            direction = button.obj['selectboxbutton']
            self.index += direction

    def hold(self, button, event):
        if self.active:
            diff = time.time() % 1000 - button.obj.get('click_time', time.time())
            if diff > 0.25:
                direction = button.obj['selectboxbutton']
                self.index += direction

    def onchange(self, obj, val, selectlist):
        '''Stand-in function for changes'''
        pass


    @property
    def value(self):
        '''Returns the contents of the text cell'''
        raw = node.get_gui_element(self.obj).value
        return raw

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, val):
        if self.selectlist:
            self._index = node.wrap(val, 0, len(self.selectlist)-1)
            entry = self.selectlist[self._index]
            if node.get_gui_element(self.obj) is not None:
                node.get_gui_element(self.obj).value = entry
            else:
                self.obj['value'] = self.selectlist[self._index]
        else:
            self._index = val


        self.onchange(self, self._index, self.selectlist)
