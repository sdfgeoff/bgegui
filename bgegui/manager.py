'''This module contains the part of BGUI that oversees a scene'''

import bge
import mathutils

from . import events


SCENE_MANAGERS = dict()
ASPECT = bge.render.getWindowHeight() / bge.render.getWindowWidth()


def get_scene_manager(scene):
    '''returns the scene manager for a given scene name'''
    return SCENE_MANAGERS.get(scene.name)


def register_node(node):
    '''Registers a node to the scene it is in'''
    manager = get_scene_manager(node.scene)
    if manager is not None:
        if manager.scene.invalid:  # Scene no longer exists
            manager = GUISceneManager(node.scene)
        manager.register_node(node)
    else:
        manager = GUISceneManager(node.scene)
        manager.register_node(node)


def remove_node(node):
    '''Removes the node so it is no longer updated. This should probably
    only be called from the nodes 'end' function'''
    manager = get_scene_manager(node.scene)
    manager.remove_node(node)


def update_manager(cont):
    '''Updates the scene's manager'''
    get_scene_manager(cont.owner.scene).auto_run()


class GUISceneManager(object):
    '''This object is for a single BGE scene, and creates the system for
    handling active objects etc. Each scene has a single active object'''
    active = None
    hover = None

    def __init__(self, scene, auto_run=True):
        SCENE_MANAGERS[scene.name] = self
        if auto_run:    # CHANGE THIS BLOCK. IT MAY CAUSE WEIRD BUGS
                        # IN OTHER PEOPLES CODE
            cont = bge.logic.getCurrentController()
            cont.script = __name__ + '.update_manager'
            cont.sensors[0].usePosPulseMode = True

        self.nodes = list()
        self.scene = scene

    def auto_run(self):
        '''Updates any nodes callbacks assuming this is a root GUI (eg an
        overlay scene). If this GUI is inside a RenderToTexture, call
        run(pos) with the UV coordinates'''
        pos = bge.logic.mouse.position
        self.run(pos)

    def set_active(self, node):
        '''Sets a node as active'''
        if self.active is not None:
            self.active.ondeactivate(node)
        self.active = node
        node.onactivate(node)

    def run(self, pos):
        '''Handles pretty much all the callbacks'''
        hit_obj, hit_uv = self.get_mouse_over(pos)

        keyheld, keypress, keyup = events.get_current_key_event()

        mouse_event = events.MouseEvent(keyheld, pos, hit_uv)
        if hit_obj is None and self.hover is not None:
            self.hover.onmouseleave()
            self.hover = None

        for node in self.nodes:
            if object_in_node(node, hit_obj):
                self.do_mouse_actions(node, mouse_event)

            if node is self.active:
                node.onactive()
                if len(keyheld) > 0:
                    node.onkeyheld(events.KeyEvent(keyheld, keyheld))
                if len(keypress) > 0:
                    node.onkeypress(events.KeyEvent(keypress, keyheld))
                if len(keyup) > 0:
                    node.onkeyup(events.KeyEvent(keyup, keyheld))
            else:
                node.oninactive()

    def do_mouse_actions(self, node, mouse_event):
        '''Fires events for the a node based on the mouse's motion'''
        # Check what object the mouse was over previously so that we can
        # detect mouseenter mouseleave events
        if self.hover is not node:
            if self.hover is not None:
                self.hover.onmouseleave()
            self.hover = node
            node.onmouseenter()
        node.onmouseover(mouse_event)

        # Do the click/mousepress
        click_status = bge.logic.mouse.events[bge.events.LEFTMOUSE]
        if click_status == bge.logic.KX_INPUT_ACTIVE:
            self.set_active(node)
            node.onmousedown(mouse_event)
        if click_status == bge.logic.KX_INPUT_JUST_ACTIVATED:
            node.onclick(mouse_event)
        if click_status == bge.logic.KX_INPUT_JUST_RELEASED:
            node.onmouseup(mouse_event)

    def get_mouse_over(self, pos):
        '''Returns the object the mouse is over'''
        cam = self.scene.active_camera
        ray_to, ray_from = get_camera_ray(cam, pos)
        res = cam.rayCast(ray_to, ray_from, 100, "", 0, 0, 2)
        hit_obj = res[0]
        hit_uv = res[4]
        return hit_obj, hit_uv

    def register_node(self, node):
        '''Registers a node as part of this scene'''
        self.nodes.append(node)

    def remove_node(self, node):
        '''De-registers a node as part of this scene'''
        self.nodes.remove(node)


def get_camera_ray(cam, coords):
    '''Returns start and end coordinates for a ray from a camera'''
    if cam.perspective:
        vect = cam.getScreenVect(*coords)
        ray_to = cam.worldPosition - vect
        ray_from = cam.worldPosition
    else:
        # GetScreenVect is broken for orthographic cameras, so we have to
        # Convert the mouse pos into a vector ourselves

        # Note inversion of Y axis because mouse coordinates are zero in
        # top left
        raw = mathutils.Vector([coords[0], -coords[1], 0])
        raw -= mathutils.Vector([0.5, -0.5, 0.0])  # Center screen is zero

        # Adjust for aspect ratio and camera zoom:
        width = cam.ortho_scale
        height = width * ASPECT
        raw.x *= width
        raw.y *= height

        pos_offset = cam.getAxisVect(raw)
        raw.z = -1
        target_offset = cam.getAxisVect(raw)
        ray_to = cam.worldPosition + target_offset
        ray_from = cam.worldPosition + pos_offset
    return ray_to, ray_from


def string_to_function(raw_str):
    '''Converts a python string of the form module.function into something
    that is callable.

    SHOULD ADD ERROR HANDLING TO THIS'''
    if not isinstance(raw_str, str):
        return raw_str  # If it's not a string, assume it's callable as is
    import importlib
    function_name = raw_str.split('.')[-1]
    module_name = '.'.join(raw_str.split('.')[:-1])
    module = importlib.import_module(module_name)
    return getattr(module, function_name)


def object_in_node(node, obj):
    '''Returns True if the object is contained as part of the node'''
    return obj is not None and obj is node.obj or \
        node.obj.groupMembers is not None and \
        obj in node.obj.groupMembers
