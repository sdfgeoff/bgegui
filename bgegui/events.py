import bge
import mathutils


class MouseEvent(object):
    '''Represents an action of the mouse on an object'''
    def __init__(self, button_data, mouse_pos, uv_pos):
        self._buttons = tuple(button_data)
        self._mouse_pos = mouse_pos
        self._uv_pos = uv_pos

    @property
    def mousePos(self):
        # In a property to make it immutable
        return self._mouse_pos.copy()

    @property
    def uvPos(self):
        # In a property to make it immutable
        return self._uv_pos.copy()

    @property
    def altKey(self):
        return bge.events.LEFTALTKEY in self._buttons or \
                bge.events.RIGHTALTKEY in self._buttons

    @property
    def ctrlKey(self):
        return bge.events.LEFTCTRLKEY in self._buttons or \
                bge.events.RIGHTCTRLKEY in self._buttons

    @property
    def shiftKey(self):
        return bge.events.LEFTSHIFTKEY in self._buttons or \
                bge.events.RIGHTSHIFTKEY in self._buttons


class KeyEvent(object):
    def __init__(self, buttons, held_buttons):
        '''Represents a list of keyboard actions in a simimlar way to the
        javascript DOM.
        Needs the held buttons allways so it can detect modifier keys'''
        self._buttons = tuple(buttons)
        self._held_buttons = held_buttons

    @property
    def altKey(self):
        return bge.events.LEFTALTKEY in self._held_buttons or \
                bge.events.RIGHTALTKEY in self._held_buttons

    @property
    def ctrlKey(self):
        return bge.events.LEFTCTRLKEY in self._held_buttons or \
                bge.events.RIGHTCTRLKEY in self._held_buttons

    @property
    def shiftKey(self):
        return bge.events.LEFTSHIFTKEY in self._held_buttons or \
                bge.events.RIGHTSHIFTKEY in self._held_buttons

    @property
    def buttons(self):
        # In a property to make it immutable
        return self._buttons



def get_current_key_event():
    '''returns the held keys, the pressed keys and the just released keys'''
    keyheld = list()
    keypress = list()
    keyup = list()
    current_events = bge.logic.keyboard.events
    for event in current_events:
        if current_events[event] == bge.logic.KX_INPUT_ACTIVE:
            keyheld.append(event)
        if current_events[event] == bge.logic.KX_INPUT_JUST_ACTIVATED:
            keypress.append(event)
        if current_events[event] == bge.logic.KX_INPUT_JUST_RELEASED:
            keyup.append(event)
    return keyheld, keypress, keyup
