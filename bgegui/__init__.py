'''This package provides a bunch of utilities for creating GUI elements.
Each element is based around a base 'node' which has:
 - A scale and position (in blender co-ordinates)
 - A bunch of assignable callbacks for:
     - mouse hover (obj, coordinates)
     - mouse enter (obj, coordinates)
     - mouse leave (obj)
     - every frame when it is the active element (obj)
     - key presses/mouse buttons when it is the active element (obj, keys)
'''
from . import manager
from . import node

def init(cont):
    '''This initializes the GUI system for this scene. It is attached to this
    scene in every way. Pausing this scene will pause the GUI elements in this
    scene'''
    cont.owner['GUISCENEMANAGER'] = manager.GUISceneManager(cont.owner.scene)
    cont.script = __name__ + '.run'


def run(cont):
    '''Updates this scene's manager'''
    cont.owner['GUISCENEMANAGER'].run()
