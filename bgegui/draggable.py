'''The draggable is a GUI element that can be clicked on and dragged. It
has very little configurability

A draggable should be UV mapped such that 1 unit of movement in the UV map is
equivalent to 1 unit of movement in localspace. The simpelest is just a 1x1
plane. Draggables move on the X-Y plane.

There are no callbacks/configurables provided by teh draggable. In the future
an 'active' property and an 'ondrag' callback may be created.
'''

from . import node


def make_draggable(cont):
    '''Makes the object draggable using the mouse'''
    Draggable(cont.owner)


class Draggable(object):
    '''An object that can be dragged with the mouse.'''
    def __init__(self, obj):
        callbacks = {
            'onmousedown': self.drag,
            'onmouseup': self.stopdrag,
            'onclick': self.startdrag,
            'onmouseleave': self.stopdrag,
        }
        self.node = node.EmptyNode(obj, callbacks)
        self.obj = obj
        if self.obj.groupObject is not None:
            self.obj.groupObject.setParent(self.obj)
        self.click_pos = None

    def startdrag(self, _, mouse_event):
        '''Stores the mouse click pos it can be matched later'''
        self.click_pos = mouse_event.uvPos

    def drag(self, _, mouse_event):
        '''Run when the mouse is clicked on the draggable'''
        if self.click_pos is None:
            return
        delta = self.click_pos - mouse_event.uvPos
        self.obj.localPosition.x -= delta.x * self.obj.worldScale.x
        self.obj.localPosition.y -= delta.y * self.obj.worldScale.y

    def stopdrag(self, *_):
        '''Stops the drag until the mouse is clicked again'''
        self.click_pos = None
