'''This file contains the base class for any GUI element

Each GUI element can have various actions associated with it. These are bound
through callbacks in one of two ways.

'''
from . import manager

COMPONENTS_STORAGE_LOCATION = 'GUIELEMENT'

MOUSE_CALLBACKS = [
    'onclick',
    'ondblclick',  # unimplemented
    'onmousedown',
    'onmouseenter',
    'onmouseleave',
    'onmousemove',
    'onmouseover',
    'onmouseup',
]


KEYBOARD_CALLBACKS = [
    'onkeyheld',
    'onkeypress',
    'onkeyup',
]

MISC_CALLBACKS = [
    'onactive',
    'onactivate',
    'oninactive',
    'ondeactivate',
]


def set_gui_element_class(obj, self):
    '''Stores an element in the object. This should be part of every make_node
    like function'''
    if obj.groupObject is not None:
        obj.groupObject[COMPONENTS_STORAGE_LOCATION] = self
    obj[COMPONENTS_STORAGE_LOCATION] = self


def is_gui_element(obj):
    '''Returns True if the object is a GUI component'''
    return COMPONENTS_STORAGE_LOCATION in obj


def get_gui_element(obj):
    '''Returns the GUI component or None'''
    return obj.get(COMPONENTS_STORAGE_LOCATION)


def make_node(cont):
    '''Makes the object calling this into an empty node'''
    new_node = EmptyNode(cont.owner)
    set_gui_element_class(cont.owner, new_node)


def get_properties_from_object(obj, property_list):
    '''Returns properties from an object that match a specified list'''
    out_dict = dict()
    for prop in property_list:
        if prop in obj:
            out_dict[prop] = obj[prop]
    return out_dict


def set_attrs(self, callback_names, settable_values):
    for settable in settable_values:
        if settable in self.obj:
            setattr(self, settable, self.obj[settable])
        if self.obj.groupObject is not None:
            if settable in self.obj.groupObject:
                setattr(self, settable, self.obj.groupObject[settable])
    for callback in callback_names:
        if callback in self.obj:
            funct = manager.string_to_function(self.obj[callback])
            setattr(self, callback, funct)
        if self.obj.groupObject is not None:
            if callback in self.obj.groupObject:
                funct_str = self.obj.groupObject[callback]
                funct = manager.string_to_function(funct_str)
                setattr(self, callback, funct)

class EmptyNode(object):
    '''This is an empty node that interacts with the mouse at a very primitive
    level'''
    valid_callbacks = MOUSE_CALLBACKS + KEYBOARD_CALLBACKS + MISC_CALLBACKS

    def __init__(self, base_obj, callbacks=None):
        self.scene = base_obj.scene
        self.obj = base_obj

        # Load callbacks in order of priority. Lowest is the actual object
        # as this is normally linked in
        # slightly higher priority is the group object
        # and at the top are ones specified for this exact object in the
        # initilization parameters
        self.callbacks = dict()
        self.load_callbacks_from_properties(self.obj)
        if self.obj.groupObject is not None:
            self.load_callbacks_from_properties(self.obj.groupObject)
        if callbacks is not None:
            self.callbacks.update(callbacks)

        manager.register_node(self)
        self.manager = manager.get_scene_manager(self.scene)

    def load_callbacks_from_properties(self, obj=None):
        '''Loads callbacks from game properties so you can create interactive
        nodes from a GUI'''
        if obj is None:
            obj = self.obj
        prop_dict = get_properties_from_object(obj, self.valid_callbacks)
        for prop in prop_dict:
            command = prop_dict[prop]
            funct = manager.string_to_function(command)
            self.callbacks[prop] = funct

    def end(self):
        '''Removes the node'''
        manager.remove_node(self)

    def __getattr__(self, attr):
        if attr in dir(self):
            return getattr(self, attr)
        if attr in self.callbacks:
            return lambda *args: self.callbacks[attr](self, *args)
        elif attr in self.valid_callbacks:
            return self.unbound

    def __setattr__(self, attr, val):
        if attr in self.valid_callbacks:
            self.callbacks[attr] = val
        else:
            self.__dict__[attr] = val

    def unbound(self, *args):
        '''Runs when there is no callback to handle the event'''



def wrap(num, minimum=-1, maximum=1):
    '''wraps the number around'''
    if maximum <= minimum:
        return -1
    while num > maximum:
        num -= (maximum - minimum) + 1
    while num < minimum:
        num += (maximum - minimum) + 1
    return num
