'''The numberbox offers a way of selecting a value, either by typing into the
entry field or by using two buttons'''

import time

from . import node

SETTABLE_VALUES = [
    'numbertype',  # Should be either 'float' or 'int'
    'value',  # The initial text of the object
    'active',  # If it can be edited
    'stepsize',  # How much a button press changes the value by
]

CALLBACKS = [
    'onchange',  # Get's run whenever the value changes
]

def to_float(x):
    '''Converts string into float for display in the numberbox'''
    if x == '':
        x = 0
    if x == '-':
        x = 0
    return round(float(x), 4)

def to_int(x):
    '''Converts string into int for display in the numberbox'''
    return int(to_float(x))


MODES = {
    'int': to_int,  # Add 0 so that it doesn't fail
    'float': to_float,  # on empty string
}


def make_numberbox(cont):
    '''Turns an object into a numberbox'''
    new_widget = NumberBox(cont.owner)
    node.set_gui_element_class(cont.owner, new_widget)


class NumberBox(object):
    stepsize = 1
    active = True
    _input_type = 'float'

    def __init__(self, obj):
        self.buttons = list()
        self.obj = obj
        self.input_type = 'float'
        for child in obj.children:
            if 'numberboxbutton' in child:
                button_callbacks = {
                    'onclick': self.press,
                    'onmousedown': self.hold
                }
                new_node = node.EmptyNode(child, button_callbacks)
                self.buttons.append(new_node)
        node.set_attrs(self, CALLBACKS, SETTABLE_VALUES)
        if self.obj.groupObject is not None:
            self.obj.setParent(self.obj.groupObject)
        if node.get_gui_element(self.obj) is not None:
            node.get_gui_element(self.obj).onchange = self.typed
        else:
            self.obj['onchange'] = self.typed

    def press(self, button, event):
        '''Runs when the mouse button is pressed on one of the buttons'''
        if self.active:
            button.obj['click_time'] = (time.time() % 1000)
            direction = button.obj['numberboxbutton']
            self.value += direction * self.stepsize

    def hold(self, button, event):
        if self.active:
            diff = time.time() % 1000 - button.obj.get('click_time', time.time())
            if diff > 0.25:
                direction = button.obj['numberboxbutton']
                self.value += direction * self.stepsize

    def typed(self, obj, val):
        '''Runs when the value of the textbox changes'''
        self.onchange(self, MODES[self.numbertype](val))

    def onchange(self, obj, val):
        '''Stand-in function for changes'''
        pass

    @property
    def numbertype(self):
        return self._input_type

    @numbertype.setter
    def numbertype(self, val):
        self._input_type = val
        if node.get_gui_element(self.obj):
            node.get_gui_element(self.obj).inputtype = val
        else:
            self.obj['inputtype'] = val

    @property
    def value(self):
        '''Returns the contents of the text cell'''
        raw = node.get_gui_element(self.obj).value
        return MODES[self.numbertype](raw)

    @value.setter
    def value(self, val):
        val = MODES[self.numbertype](val)
        if node.get_gui_element(self.obj):
            node.get_gui_element(self.obj).value = val
        else:
            self.obj['value'] = val

