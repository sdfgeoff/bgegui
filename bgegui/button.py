'''A button is a simplified element that offers onclick as a callback
It also offers the 'text' property so you can set what it says.
'''
'''This module provides support for text entry boxes. The text entry box
has support for various modes including:
 - Text only
 - Text and numbers only
 - Numbers only
 - Integers only
'''
import bge
from . import node


SETTABLE_VALUES = [
    'text',  # The initial text of the object
]

CALLBACKS = [
    'onclick',  # Get's run whenever the value changes
]

def make_button(cont):
    '''Makes the current object into a text entry line'''
    node.set_gui_element_class(cont.owner, ButtonWidget(cont.owner))


class ButtonWidget(object):
    '''A text widget that can be clicked on an typed into'''
    def __init__(self, obj):
        self.obj = obj
        self.node = node.EmptyNode(obj)
        self.text_obj = None
        for child in obj.children:
            if isinstance(child, bge.types.KX_FontObject):
                self.text_obj = child
        node.set_attrs(self, CALLBACKS, SETTABLE_VALUES)
        self.node.onclick = self._onclick_callback
        if self.obj.groupObject is not None:
            self.obj.setParent(self.obj.groupObject)

    def _onclick(self, node, event):
        '''Dummy onclick'''

    def _onclick_callback(self, node, event):
        self.onclick(self, event)

    @property
    def text(self):
        return self.text_obj.text

    @text.setter
    def text(self, val):
        self.text_obj.text = str(val)

    @property
    def onclick(self):
        return self._onclick

    @onclick.setter
    def onclick(self, val):
        self._onclick = val
