'''
The slider toggle is like a checkbox, but it slides between on and off.
You can set the text it displays
'''

import time

from . import node

SETTABLE_VALUES = [
    'value',  # True or False
    'text',  # What to display in the slider window
    'active'
]

CALLBACKS = [
    'onchange',  # Get's run whenever the value changes
]

def make_slidertoggle(cont):
    '''Turns an object into a numberbox'''
    new_widget = SliderToggle(cont.owner)
    node.set_gui_element_class(cont.owner, new_widget)


class SliderToggle(object):
    _index = 0
    active = True
    def __init__(self, obj):
        self.buttons = list()
        self.obj = obj
        self.selectlist = list()
        callbacks = {
            'onclick': self.toggle
        }
        self._state = False

        self.node = node.EmptyNode(obj, callbacks)
        node.set_attrs(self, CALLBACKS, SETTABLE_VALUES)
        if isinstance(self.selectlist, str):
            self.selectlist = self.selectlist.split(',')

        if self.obj.groupObject is not None:
            self.obj.parent.parent.setParent(self.obj.groupObject)

        #self.text = self.text

    def toggle(self, button, event):
        '''Runs when the mouse button is pressed on the other side'''
        if self.active:
            self.value = not self.value

    def hold(self, button, event):
        if self.active:
            diff = time.time() % 1000 - button.obj.get('click_time', time.time())
            if diff > 0.25:
                direction = button.obj['selectboxbutton']
                self.index += direction

    def onchange(self, obj, val):
        '''Stand-in function for changes'''
        pass

    @property
    def value(self):
        '''Returns the contents of the text cell'''
        raw = self._state
        return raw

    @value.setter
    def value(self, val):
        self._state = val
        self.obj.parent.worldOrientation = [0,0,3.14*val]
        self.onchange(self, val)

    @property
    def text(self):
        text_obj = self.obj.parent.parent
        if node.get_gui_element(text_obj) is not None:
            return node.get_gui_element(text_obj).value
        return "ERR"

    @text.setter
    def text(self, val):
        text_obj = self.obj.parent.parent
        if node.get_gui_element(text_obj) is not None:
            node.get_gui_element(text_obj).value = val
        else:
            text_obj['value'] = val
