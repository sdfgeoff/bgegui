'''This makes a node that can be scaled. This is intended for scaling objects
with a border that doesn't scale, so it uses an armature.

This is actually independant from making something a node - but it may not stay
this way

Meshes to be used with this should
'''


def make_scale(cont):
    ScalableElement(cont.owner)


class ScalableElement(object):
    def __init__(self, armature, scale=(2, 5)):
        self.armature = armature
        self.update_scale(scale[0], scale[1])

    def update_scale(self, x_size, y_size):
        for channel in self.armature.channels:
            loc = channel.location
            if channel.name.find('XY') != -1:
                loc.x = -x_size
                loc.y = y_size
                channel.location = loc
                self.armature.update()
        for child in self.armature.children:
            child.reinstancePhysicsMesh()

